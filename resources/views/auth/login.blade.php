

    <!DOCTYPE html>
    <html lang="en" class="material-style layout-fixed">
    
    <head>
        <title>soengsouy.com</title>
    
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="Empire Bootstrap admin template made using Bootstrap 4, it has tons of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
        <meta name="keywords" content="Empire, bootstrap admin template, bootstrap admin panel, bootstrap 4 admin template, admin template">
        <meta name="author" content="soengsouy" />
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico">
    
        <!-- Google fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    
        <!-- Icon fonts -->
        <link rel="stylesheet" href="{{URL::to('assets/fonts/fontawesome.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/fonts/ionicons.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/fonts/linearicons.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/fonts/open-iconic.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/fonts/pe-icon-7-stroke.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/fonts/feather.css')}}">
    
        <!-- Core stylesheets -->
        <link rel="stylesheet" href="{{URL::to('assets/css/bootstrap-material.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/css/shreerang-material.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/css/uikit.css')}}">
    
        <!-- Libs -->
        <link rel="stylesheet" href="{{URL::to('assets/libs/perfect-scrollbar/perfect-scrollbar.css')}}">
        <!-- Page -->
        <link rel="stylesheet" href="{{URL::to('assets/css/pages/authentication.css')}}">
    </head>
    
    <body>
        <!-- [ Preloader ] Start -->
        <div class="page-loader">
            <div class="bg-primary"></div>
        </div>
        <!-- [ Preloader ] End -->
    
        <!-- [ content ] Start -->
        <div class="authentication-wrapper authentication-1 px-4">
            <div class="authentication-inner py-5">
    
                <!-- [ Logo ] Start -->
                <div class="d-flex justify-content-center align-items-center">
                    <div class="ui-w-60">
                        <div class="w-100 position-relative">
                            <img src="{{URL::to('assets/img/logo-dark.png')}}" alt="Brand Logo" class="img-fluid">
                        </div>
                    </div>
                </div>
                <br>
                <!-- [ Logo ] End -->
                @if(session()->has('error'))
                    <div class="text-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
               
                <!-- [ Form ] Start -->
                <form method="POST" action="{{ route('login') }}" class="my-5">
                    @csrf
                    <div class="form-group">
                        <label class="form-label">Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="form-label d-flex justify-content-between align-items-end">
                            <span>Password</span>
                            <a href="{{ route('forget-password') }}" class="d-block small">Forgot password?</a>
                        </label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="clearfix"></div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center m-0">
                        <label class="custom-control custom-checkbox m-0">
                            <input class="form-check-input" type="checkbox" value="" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <span class="custom-control-label">Remember me</span>
                        </label>
                        <button type="submit" class="btn btn-primary">Sign In</button>
                    </div>
                </form>
                <!-- [ Form ] End -->
    
                <div class="text-center text-muted">
                    Don't have an account yet?
                    <a href="{{ route('register') }}">Sign Up</a>
                </div>
            </div>
        </div>
        <!-- [ content ] End -->
    
        <!-- Core scripts -->
        <script src="{{URL::to('assets/js/pace.js')}}"></script>
        <script src="{{URL::to('assets/js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{URL::to('assets/libs/popper/popper.js')}}"></script>
        <script src="{{URL::to('assets/js/bootstrap.js')}}"></script>
        <script src="{{URL::to('assets/js/sidenav.js')}}"></script>
        <script src="{{URL::to('assets/js/layout-helpers.js')}}"></script>
        <script src="{{URL::to('assets/js/material-ripple.js')}}"></script>
    
        <!-- Libs -->
        <script src="{{URL::to('assets/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    
        <!-- Demo -->
        <script src="{{URL::to('assets/js/demo.js')}}"></script>
        <script src="{{URL::to('assets/js/analytics.js')}}"></script>
    </body>
    
    </html>
    